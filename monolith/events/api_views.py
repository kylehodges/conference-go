from django.http import JsonResponse
import json
from common.json import ModelEncoder, DateEncoder
from .models import Conference, Location, State
from .acls import get_photo, get_weather_data
from django.views.decorators.http import require_http_methods

class ConferenceListEncoder(ModelEncoder):
     model = Conference
     properties = [
          'name',
     ]


@require_http_methods(['GET', 'POST'])
def api_list_conferences(request):
    if request.method == 'GET':
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location = Location.objects.get(id=content['location'])
            content['location'] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

class LocationListEncoder(ModelEncoder):
     model = Location
     properties = ['name']

class LocationDetailEncoder(ModelEncoder):
     model = Location
     properties = [
          'name',
          'city',
          'room_count',
          'created',
          'updated',
          'photo'
     ]

     def get_extra_data(self, o):
          return {'state': o.state.abbreviation}

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
         'location': LocationListEncoder(),
    }


@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_show_conference(request, id):
    if request.method == 'GET':
        conference = Conference.objects.get(id=id)
        city = conference.location.city
        state = conference.location.state
        weather = get_weather_data(city, state)
        return JsonResponse(
             {"conference": conference, "weather": weather},
             encoder=ConferenceDetailEncoder,
             safe=False
        )
    elif request.method == 'DELETE':
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=id)
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"}
            )
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

@require_http_methods(['GET', 'POST'])
def api_list_locations(request):
    if request.method == 'GET':
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content['state'])
            content['state'] = state
        except State.DoesNotExist:
             return JsonResponse(
                  {"message": "Invalid state abbreviation"},
                  status=400,
             )
        content['photo'] = get_photo(content['city'], content['state'])
        location = Location.objects.create(**content)
        return JsonResponse(
              location,
              encoder=LocationDetailEncoder,
              safe=False,
         )

@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_show_location(request, id):

    if request.method == 'GET':
        location = Location.objects.get(id=id)

        return JsonResponse(
                location,
                encoder=LocationDetailEncoder,
                safe=False
            )
    elif request.method == 'DELETE':
         count, _ = Location.objects.filter(id=id).delete()
         return JsonResponse({"deleted": count > 0})
    else:
    # copied from create
        content = json.loads(request.body)
        try:
            # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # new code
        Location.objects.filter(id=id).update(**content)

        # copied from get detail
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
