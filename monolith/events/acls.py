from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):
    url = f'https://api.pexels.com/v1/search?query={city}+{state}&per_page=1'
    headers = {'Authorization': PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    content = json.loads(response.text)
    return content["photos"][0]['url']


def get_weather_data(city, state):
    geou = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit={1}&appid={OPEN_WEATHER_API_KEY}"
    latlong = requests.get(geou)
    ll_content = json.loads(latlong.text)
    lat = ll_content[0]["lat"]
    lon = ll_content[0]["lon"]
    url = f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial'

    response = requests.get(url)
    content = json.loads(response.text)
    description = content["weather"][0]["description"]
    temp = content["main"]["temp"]
    return {"description": description, "temp": temp}
